package de.crafter75.configutil;

public enum ConfigMode {
    DEFAULT,
    PATH_BY_UNDERSCORE,
    FIELD_IS_KEY
}
